<?php
    // Array Assosiative = Array yang indeksnya menggunakan string yang diasosiasikan
    // "key" 
    // => : untuk mendifinisikan key
    // "Isi key"
    // Array Assosiative bebas mendefinisikan isinya di posisi manapun asalkan indeksnya benar
    $pasien = [
        [
            "nama" => "Ahmad", 
            "gender" => "Pria", 
            "alamat" => "Pasir Kuda, Bogor Barat", 
            "no_hp" => "085155279686",
            "gambar" => "ahmad.jpg"
        ],
        [
            "nama" => "Udin", 
            "gender" => "Pria", 
            "alamat" => "Bogor Nirwana Residence, Bogor Selatan", 
            "no_hp" => "087711356758",
            "gambar" => "udin.jpg"
        ],
        [
            "nama" => "Putri", 
            "gender" => "Wanita", 
            "alamat" => "Rangga Mekar, Bogor Selatan", 
            "no_hp" => "085155279686",
            "gambar" => "putri.jpg"
        ]
    ]; 
?>

<html>
<head>
    <title>Daftar Pasien</title>
</head>
<body>
    <h1>Daftar Pasien</h1>
    
    <?php foreach ($pasien as $ps) : ?>
    <ul>
        <li><img src="img/<?= $ps["gambar"]; ?>" width="100px"  alt=""></li>
        <li>Nama : <?= $ps["nama"]; ?></li>
        <li>Jenis Kelamin : <?= $ps["gender"]; ?></li>
        <li>Alamat : <?= $ps["alamat"]; ?></li>
        <li>No. Hp : <?= $ps["no_hp"]; ?></li>
    </ul>
    <?php endforeach; ?>
</body>
</html>