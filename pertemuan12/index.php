<?php
    // Memanggil/menghubungkan dengan file functions.php
        require 'functions.php'; 

    // Ambil data dari tabel mahasiswa / query data mahasiswa menggunakan fungsi query yang ada pada file functions.php    
        $mahasiswa = query("SELECT * FROM mahasiswa ORDER BY id ASC");
        /**
         * $mahasiswa = membuat variabel array multidimensi
         * query = memanggil fungsi query, fungsi disimpan pada file functions.php dan sudah dihubungkan menggunakan require diatas, jadi dapat kita gunakan
         * ("SELECT * FROM mahasiswa") = parameter fungsi berbentuk string yang akan disimpan pada variabel $query yang ada pada fungsi query
         * ORDER BY = Urutkan berdasarkan
         * id = field id
         * ASC = Ascending (dari kecil ke besar), jika sebaliknya maka DESC "Descending" (dari besar ke kecil)
         */

    // Tombol cari ditekan
        if (isset($_POST['cari'])) {
            $mahasiswa = cari($_POST['keyword']);
        }
        /**
         * Jika tombol cari ditekan
         * variabel mahasiswa akan berisikan nilai/hasil yang didapatkan oleh fungsi cari dengan parameter berupa nilai dari kotak input pencarian
         */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>

    <a href="tambah.php">Tambah Data Mahasiswa</a>

    <br><br>
    <form action="" method="post">
        <input type="text" name="keyword" id="keyword" size="40" placeholder="Masukkan keyword pencarian" autofocus autocomplete="off">
        <!--
         size : mengatur ukuran kotak input
         placeholder : mengisi teks bantuan di dalam kotak input
         autofocus : agar ketika pengguna membuka halaman web, kotak input otomatis aktif (jika mengetik otomatis masuk di kotak input tanpa harus di klik terlebih dahulu)
         autocomplete="off" : berfungsi mematikan saran pencarian, berguna agar histori pencarian tidak ditampilkan
         -->
        <button type="submit" name="cari">Cari</button>
    </form>

    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Gambar</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Jurusan</th>
        </tr>
        <?php $i = 1;?>
        <?php foreach($mahasiswa as $row) : ?>
        <tr>
            <td><?= $i ?></td>
            <td>
                <a href="ubah.php?id=<?php echo $row["id"]; ?>">Ubah</a> |
                <a href="hapus.php?id=<?php echo $row["id"]; ?>" onclick = "return confirm('Yakin?');" >Hapus</a>
            </td>
            <td>
                <img src="img/<?= $row["gambar"] ?>" alt="<?= $row["gambar"] ?>" width="150">
            </td>
            <td><?= $row["nrp"] ?></td>
            <td><?= $row["nama"] ?></td>
            <td><?= $row["email"] ?></td>
            <td><?= $row["jurusan"] ?></td>
        </tr>
        <?php $i++ ?>
        <?php endforeach; ?>
    </table>
</body>
</html>