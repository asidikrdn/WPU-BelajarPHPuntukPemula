<?php
    // File ini berisi kumpulan fungsi yang bisa digunakan di file lain dengan memanggil menggunakan require
    // Hal ini dilakukan agar kode lebih rapih dan modular, serta mempercepat penulisan yang membutuhkan fungsi yang sama

    // Koneksi ke database
        $conn = mysqli_connect("localhost", "root", "", "phpdasar");
    
    // Membuat functions query
        function query($query){
            global $conn;
            $result=mysqli_query($conn, $query);
            $rows=[];
            while($row = mysqli_fetch_assoc($result) ) {
                $rows[] = $row;
            }
            return $rows;
        }

    // Membuat function tambah data
        function tambah($data){
            $nrp = htmlspecialchars($data["nrp"]);
            $nama = htmlspecialchars($data["nama"]);
            $email = htmlspecialchars($data["email"]);
            $jurusan = htmlspecialchars($data["jurusan"]);
            $gambar = htmlspecialchars($data["gambar"]);

            global $conn;

            $query = "INSERT INTO mahasiswa VALUES ('','$nrp','$nama', '$email', '$jurusan', '$gambar')";
            mysqli_query($conn, $query);
            return mysqli_affected_rows($conn);
        }

    // Membuat fungtion hapus data 
        function hapus($id){
            global $conn;
            $query = "DELETE FROM mahasiswa WHERE id = $id";
            mysqli_query($conn, $query);
            return mysqli_affected_rows($conn);
        }

    // Membuat function ubah data
        function ubah($data){
            $id = $data['id'];
            $nrp = htmlspecialchars($data["nrp"]);
            $nama = htmlspecialchars($data["nama"]);
            $email = htmlspecialchars($data["email"]);
            $jurusan = htmlspecialchars($data["jurusan"]);
            $gambar = htmlspecialchars($data["gambar"]);

            global $conn;

                $query = "UPDATE mahasiswa SET
                        nrp = '$nrp',
                        nama = '$nama', 
                        email = '$email', 
                        jurusan = '$jurusan', 
                        gambar = '$gambar'
                        WHERE id = $id
                        ";

            mysqli_query($conn, $query);
            return mysqli_affected_rows($conn);
        }

    // Membuat fungsi cari
        function cari($keyword){        // Membuat fungsi keyword dan menyimpan parameter yang ditangkap berupa $_POST['keyword] kedalam variabel $keyword
            $query = "SELECT * FROM mahasiswa
                    WHERE 
                    nrp LIKE '%$keyword%'
                    OR
                    nama LIKE '%$keyword%'
                    OR
                    email LIKE '%$keyword%'
                    OR
                    jurusan LIKE '%$keyword%'
                    ";
            /**
             * $query : membuat variabel query yang berisikan string query agar mudah saat dipanggil
             * "" : string query 
             * SELECT : Pilih 
             * * : SemuaData
             * FROM : Dari
             * mahasiswa : tabel_mahasiswa
             * WHERE Dimana
             * nrp, nama, email, jurusan, gambar : field yang diidentifikasi
             * LIKE : menyerupai (tidak menggunakan '=' karena '=' itu hasilnya harus mutlak sama) 
             * % : wildcard, melengkapi yang tidak tertuliskan. Misal kita mencari Rudini, jika kita menuliskan 'udin' saja maka 'R' dan 'ini' tidak diwajibkan ditulis, akan diabaikan dan otomatis dilengkapi oleh wildcard ini
             * $keyword : nilai_dari_variabel_$keyword
             * OR : Atau
             */

            return query($query);   // lalu kembalikan nilai dari fungsi query (fungsi querynya sudah dibuat sebelumnya, ada di atas) yang parameternya berupa isi dari variabel $query
        }
?>