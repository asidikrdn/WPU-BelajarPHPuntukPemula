<?php
    // Koneksi ke file functions.php
    require 'functions.php';

    // Menampung nilai variabel $_GET['id] yang terkandung di url kedalam variabel baru bernama $id 
    $id = $_GET['id'];

    // Ambil data berdasarkan id dari tabel mahasiswa / query data mahasiswa menggunakan fungsi query yang ada pada file functions.php    
    $mahasiswa = query("SELECT * FROM mahasiswa WHERE id = $id")[0];

    // Cek tombol submit sudah di klik atau belum
    if ( isset($_POST["submit"]) ){

        // Cek apakah data berhasil diubah atau tidak
        // menggunakan fungsi ubah yang sudah dibuat, dimana dalam fungsinya sudah ada nilai return dari fungsi mysqli_affected_rows()
        if (ubah($_POST) > 0 ){
            echo "
                <script>
                    alert('Data berhasil diubah !');
                    document.location.href = 'index.php';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('Data gagal diubah !');
                    document.location.href = 'index.php';
                </script>
            ";
        }

    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Data Mahasiswa</title>
</head>
<body>
    <h1>Update Data Mahasiswa</h1>

    <form action="" method="post">
        <input type="hidden" name="id" value="<?php echo $mahasiswa["id"]; ?>">
        <ul>
            <li>
                <label for="nrp">NRP : </label>
                <input type="text" name="nrp" id="nrp" required value="<?php echo $mahasiswa["nrp"] ?>" > 
            </li>
            <li>
                <label for="nama">Nama : </label>
                <input type="text" name="nama" id="nama" required value="<?php echo $mahasiswa["nama"] ?>">
            </li>
            <li>
                <label for="email">Email : </label>
                <input type="text" name="email" id="email" required value="<?php echo $mahasiswa["email"] ?>">
            </li>
            <li>
                <label for="jurusan">Jurusan : </label>
                <input type="text" name="jurusan" id="jurusan" required value="<?php echo $mahasiswa["jurusan"] ?>">
            </li>
            <li>
                <label for="gambar">Gambar : </label>
                <input type="text" name="gambar" id="gambar" required value="<?php echo $mahasiswa["gambar"] ?>">
            </li>
            <li>
                <button type="submit" name="submit">Update Data</button>
            </li>
        </ul>
    </form>
</body>
</html>