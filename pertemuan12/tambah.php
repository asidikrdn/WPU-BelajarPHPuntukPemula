<?php
    // Koneksi ke file functions.php
    require 'functions.php';

    // Cek tombol submit sudah di klik atau belum
    if ( isset($_POST["submit"]) ){
        
        // Cek apakah data berhasil ditambahkan atau tidak
        // menggunakan fungsi tambah yang sudah dibuat, dimana dalam fungsinya sudah ada nilai return dari fungsi mysqli_affected_rows()
        if (tambah($_POST) > 0 ){
            echo "
                <script>
                    alert('Data berhasil ditambahkan !');
                    document.location.href = 'index.php';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('Data gagal ditambahkan !');
                    document.location.href = 'index.php';
                </script>
            ";
        }

    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Data Mahasiswa</title>
</head>
<body>
    <h1>Tambah Data Mahasiswa</h1>

    <form action="" method="post"> <!-- action dikosongkan karena form ini akan mengirim data ke halaman ini sendiri -->
        <ul>
            <li>
                <label for="nrp">NRP : </label> <!-- for : menandakan label milik siapa, biasanya diisikan sesuai id-->
                <input type="text" name="nrp" id="nrp" required> <!-- name : nama variabel/keyword objek yang akan menyimpan inputan; id : identifier, biasanya digunakan untuk for pada label -->
            </li>
            <li>
                <label for="nama">Nama : </label>
                <input type="text" name="nama" id="nama" required> <!-- required = atribut html 5 yang berfungsi mewajibkan pengguna untuk mengisi form yang ada (tidak boleh dikosongkan) -->
            </li>
            <li>
                <label for="email">Email : </label>
                <input type="text" name="email" id="email" required>
            </li>
            <li>
                <label for="jurusan">Jurusan : </label>
                <input type="text" name="jurusan" id="jurusan" required>
            </li>
            <li>
                <label for="gambar">Gambar : </label>
                <input type="text" name="gambar" id="gambar" required>
            </li>
            <li>
                <button type="submit" name="submit">Tambah Data</button>
            </li>
        </ul>
    </form>
</body>
</html>