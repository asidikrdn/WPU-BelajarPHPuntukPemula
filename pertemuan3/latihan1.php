<?php
// Pengulangan
// for
// while
// do.. while
// foreach : pengulangan khusus array

//for($i = 0; $i < 5; $i++) {
//    echo "Hello World ! <br>";
//}


// $i=0;
// while($i < 5) {
//     echo "Hello World ! <br>";
//     $i++;
// }


// $i=0;
// do {
//     echo "Hello World ! <br>";
//     $i++;
// } while($i < 5);

?>

<html>
<head>
    <title>Latihan 1</title>
    <style type="text/css">
        .warna-baris{
            background-color: silver;
        }
    </style>
</head>
<body>
    <!--
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <td>1.1</td>
            <td>1.2</td>
            <td>1.3</td>
            <td>1.4</td>
            <td>1.5</td>
        </tr>
    </table>
    -->

    <!--
    <table border="1" cellpadding="10" cellspacing="0">
        <?php
            for($i=1; $i<=3; $i++) {
                echo "<tr>";
                for($j=1; $j<=5; $j++){
                    echo "<td>$i,$j</td>";
                }
                echo "<tr>";
            }
        ?>

    </table>
    -->
    
    <!--
    <table border="1" cellpadding="10" cellspacing="0">
    <?php for($i=1; $i<=3; $i++) { ?>
        <tr>
            <?php for($j=1; $j<=5; $j++) {?>
            <td><?php echo "$i,$j"; ?></td>    
            <?php }?>
        </tr>
    <?php } ?>
    </table>
    --> 
    
    <table border="1" cellpadding="10" cellspacing="0">
    <?php for($i=1; $i<=5; $i++) : ?>
        <?php if($i %2 == 1) : ?>
        <tr class="warna-baris">
        <?php else : ?>
        <tr>
        <?php endif ?>
            <?php for($j=1; $j<=5; $j++) : ?>
            <td><?= "$i,$j"; //<?= adalah sintak pengganti echo pada php ?></td>    
            <?php endfor; ?>
        </tr>
    <?php endfor; //untuk memudahkan menutup agar tak bingung milik siapa (berlaku untuk perulangan dan percabangan) ?>
    </table>
    

</body>
</html>