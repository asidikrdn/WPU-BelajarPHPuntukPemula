<?php
    // Memanggil/menghubungkan dengan file functions.php
        require 'functions.php'; 

    // Ambil data dari tabel mahasiswa / query data mahasiswa menggunakan fungsi query yang ada pada file functions.php    
        $mahasiswa = query("SELECT * FROM mahasiswa");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>

    <a href="tambah.php">Tambah Data Mahasiswa</a>

    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Gambar</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Jurusan</th>
        </tr>
        <?php $i = 1;?>
        <?php foreach($mahasiswa as $row) : ?>
        <tr>
            <td><?= $i ?></td>
            <td>
                <a href="">Ubah</a> |
                <a href="hapus.php?id=<?php echo $row["id"]; ?>" onclick = "return confirm('Yakin?');" >Hapus</a>       <!-- onclick = fitur javascript; return = mengembalikan nilai confirm; confirm = pesan konfirmasi (bisa di klik ya atau tidak); ('Yakin?') = teks yang ditampilkan pada pesan konfirmasi; jadi apabila nilai returnnya true maka data akan lanjut dihapus, namun jika nilai returnnya false maka data tidak jadi dihapus; -->
            </td>
            <td>
                <img src="img/<?= $row["gambar"] ?>" alt="<?= $row["gambar"] ?>" width="150">
            </td>
            <td><?= $row["nrp"] ?></td>
            <td><?= $row["nama"] ?></td>
            <td><?= $row["email"] ?></td>
            <td><?= $row["jurusan"] ?></td>
        </tr>
        <?php $i++ ?>
        <?php endforeach; ?>
    </table>
</body>
</html>