<?php
    // Koneksi ke file functions.php
    require 'functions.php';

    // Menampung nilai variabel $_GET['id] yang terkandung di url kedalam variabel baru bernama $id 
    $id = $_GET['id'];

    // Menjalankan fungsi hapus, fungsi hapus ada di dalam file functions.php
    if (hapus($id) > 0){
        echo "
            <script>
                alert('Data berhasil dihapus !');   // Memberikan notifikasi pop-up yang isinya text 'Data berhasil dihapus !'
                document.location.href = 'index.php';   // Mengalihkan halaman ke index.php
            </script>
        ";
    } else {
        echo "
            <script>
                alert('Data gagal dihapus !');
                document.location.href = 'index.php';
            </script>
        ";
    }

?>