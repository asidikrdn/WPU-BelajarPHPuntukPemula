<?php
    // File ini berisi kumpulan fungsi yang bisa digunakan di file lain dengan memanggil menggunakan require
    // Hal ini dilakukan agar kode lebih rapih dan modular, serta mempercepat penulisan yang membutuhkan fungsi yang sama

    // Koneksi ke database
        $conn = mysqli_connect("localhost", "root", "", "phpdasar");
    
    // Membuat functions query
        function query($query){
            global $conn;
            $result=mysqli_query($conn, $query);
            $rows=[];
            while($row = mysqli_fetch_assoc($result) ) {
                $rows[] = $row;
            }
            return $rows;
        }

    // Membuat function tambah data
        function tambah($data){         // $data akan menyimpan parameter yang digunakan saat menjalankan fungsi tambah
            $nrp = htmlspecialchars($data["nrp"]);        // $_POST tidak kita gunakan, karena ketika kita memanggil fungsi tambah dan menggunakan $_POST sebagai parameternya, maka otomatis $_POST akan tersimpan pada $data
            $nama = htmlspecialchars($data["nama"]);      // fungsi htmlspecialchars berguna untuk mengubah kode html yang diinputkan menjadi string biasa, hal ini berguna untuk menghindari ada pengguna jail yang memasukkan kode html melalui kolom input.
            $email = htmlspecialchars($data["email"]);
            $jurusan = htmlspecialchars($data["jurusan"]);
            $gambar = htmlspecialchars($data["gambar"]);

            global $conn;       // Mendeklarasikan variabel $conn agar dapat digunakan di dalam fungsi

            // query insert data ke database
                $query = "INSERT INTO mahasiswa VALUES ('','$nrp','$nama', '$email', '$jurusan', '$gambar')";
                /**
                 *$query = variabel lokal untuk menampung string query
                 *INSERT INTO = string query untuk memasukkan data
                 *mahasiswa = nama tabel pada database
                 *VALUES = query untuk mendefinisikan nilai yang akan dimasukkan
                 *('') = setiap string di dalam kutip adalah nilai yang akan dimasukkan kedalam tabel, urutannya harus sesuai dengan urutan field pada tabel, jika tidak diisi biarkan kosong didalam kutip 
                 */
                
            
            mysqli_query($conn, $query);
            /**
             * mysqli_query = menjalankan fungsi/sintaks mysql dengan variabel $conn dan $query sebagai parameternya
             * $conn = berisi koneksi database
             * $query = berisi sintaks query
             */

            return mysqli_affected_rows($conn);     // Mengembalikan nilai mysqli_affected_rows
        }

    // Membuat fungtion hapus data 
        function hapus($id){        // Membuat fungsi hapus dengan parameter yang disimpan dalam sebuah variabel $id, variabel $id disini merupakan variabel lokal
            global $conn;       // Memanggil variabel $conn agar dapat digunakan didalam fungsi

            $query = "DELETE FROM mahasiswa WHERE id = $id";        // Membuat variabel untuk menampung string query
            mysqli_query($conn, $query);            // melakukan/menjalankan fungsi mysqli_query

            return mysqli_affected_rows($conn);     // Mengembalikan nilai mysqli_affected_rows() untuk mengetahui apakah perubahan pada database berhasil atau error
        }
?>