<?php
    // Koneksi ke file functions.php
    require 'functions.php';

    // Menampung nilai variabel $_GET['id] yang terkandung di url kedalam variabel baru bernama $id 
    $id = $_GET['id'];

    // Ambil data berdasarkan id dari tabel mahasiswa / query data mahasiswa menggunakan fungsi query yang ada pada file functions.php    
    $mahasiswa = query("SELECT * FROM mahasiswa WHERE id = $id")[0];
    /**
     * $mahasiswa = membuat variabel array multidimensi
     * query = memanggil fungsi query, fungsi disimpan pada file functions.php dan sudah dihubungkan menggunakan require diatas, jadi dapat kita gunakan
     * ("SELECT * FROM mahasiswa") = parameter fungsi berbentuk string yang akan disimpan pada variabel $query yang ada pada fungsi query
     * WHERE = dimana (mencari data dengan spesifikasi tertentu)
     * id = nama field yang ada pada tabel mahasiswa yang akan jadi acuan untuk mencari data, artinya WHERE id = data dengan id.... 
     * $id = variabel yang kita buat untuk menampung id yang ada di halaman index
     * WHERE id = $id artinya mencari data dimana (field) idnya bernilai $id 
     * [0] = mengambil hasil query baris pertama untuk dimasukkan sebagai nilai key dan valuenya kedalam array assosiative $mahasiswa.
     * jika tidak menggunakan [0], maka variabel $mahasiswa akan menjadi array multi dimensi, dimana array luarnya berupa array numeric (tiap row pada tabel yang berhasil diambil) dan array didalamnya baru berupa array assosiative yang berisi key dan value dari tiap baris data yang diambil
     * karena id merupakan primary key, maka hasil query hanya akan mendapatkan 1 baris data, sesuai dengan id yang diminta
     */

    // Cek tombol submit sudah di klik atau belum
    if ( isset($_POST["submit"]) ){

        // Cek apakah data berhasil diubah atau tidak
        // menggunakan fungsi ubah yang sudah dibuat, dimana dalam fungsinya sudah ada nilai return dari fungsi mysqli_affected_rows()
        if (ubah($_POST) > 0 ){       // Menjalankan fungsi ubah dengan $_POST sebagai parameternya
            echo "
                <script>
                    alert('Data berhasil diubah !');
                    document.location.href = 'index.php';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('Data gagal diubah !');
                    document.location.href = 'index.php';
                </script>
            ";
        }

    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Data Mahasiswa</title>
</head>
<body>
    <h1>Update Data Mahasiswa</h1>

    <form action="" method="post"> <!-- action dikosongkan karena form ini akan mengirim data ke halaman ini sendiri -->
        <input type="hidden" name="id" value="<?php echo $mahasiswa["id"]; ?>">
        <!-- hidden : salah satu tipe input yang sifatnya tersembunyi dan tidak bisa dibaca oleh pengguna
            input tipe hidden digunakan karena id disini tidak diinputkan oleh pengguna, melainkan tetap menggunakan id yang sebelumnya sudah ada
         -->
        <ul>
            <li>
                <label for="nrp">NRP : </label> <!-- for : menandakan label milik siapa, biasanya diisikan sesuai id-->
                <input type="text" name="nrp" id="nrp" required value="<?php echo $mahasiswa["nrp"] ?>" > 
                <!-- name : nama variabel/keyword objek yang akan menyimpan inputan; 
                    id : identifier, biasanya digunakan untuk for pada label
                    required = atribut html 5 yang berfungsi mewajibkan pengguna untuk mengisi form yang ada (tidak boleh dikosongkan)
                    value : atribut yang berfungsi memberikan nilai/isian default yang ada di dalam form inputnya
                     -->
            </li>
            <li>
                <label for="nama">Nama : </label>
                <input type="text" name="nama" id="nama" required value="<?php echo $mahasiswa["nama"] ?>">
            </li>
            <li>
                <label for="email">Email : </label>
                <input type="text" name="email" id="email" required value="<?php echo $mahasiswa["email"] ?>">
            </li>
            <li>
                <label for="jurusan">Jurusan : </label>
                <input type="text" name="jurusan" id="jurusan" required value="<?php echo $mahasiswa["jurusan"] ?>">
            </li>
            <li>
                <label for="gambar">Gambar : </label>
                <input type="text" name="gambar" id="gambar" required value="<?php echo $mahasiswa["gambar"] ?>">
            </li>
            <li>
                <button type="submit" name="submit">Update Data</button>
            </li>
        </ul>
    </form>
</body>
</html>