<?php
    // File ini berisi kumpulan fungsi yang bisa digunakan di file lain dengan memanggil menggunakan require
    // Hal ini dilakukan agar kode lebih rapih dan modular, serta mempercepat penulisan yang membutuhkan fungsi yang sama

    // Koneksi ke database
        $conn = mysqli_connect("localhost", "root", "", "phpdasar");
    
    // Membuat functions query
        function query($query){
            global $conn;
            $result=mysqli_query($conn, $query);
            $rows=[];
            while($row = mysqli_fetch_assoc($result) ) {
                $rows[] = $row;
            }
            return $rows;
        }
        
    // Membuat function tambah data
        function tambah($data){
            $nrp = htmlspecialchars($data["nrp"]);
            $nama = htmlspecialchars($data["nama"]);
            $email = htmlspecialchars($data["email"]);
            $jurusan = htmlspecialchars($data["jurusan"]);
            $gambar = htmlspecialchars($data["gambar"]);

            global $conn;

            // query insert data ke database
                $query = "INSERT INTO mahasiswa VALUES ('','$nrp','$nama', '$email', '$jurusan', '$gambar')";
                
            mysqli_query($conn, $query);
            return mysqli_affected_rows($conn);
        }

    // Membuat fungtion hapus data 
        function hapus($id){
            global $conn;
            $query = "DELETE FROM mahasiswa WHERE id = $id";
            mysqli_query($conn, $query);
            return mysqli_affected_rows($conn);
        }

    // Membuat function ubah data
    function ubah($data){         // $data akan menyimpan parameter yang digunakan saat menjalankan fungsi ubah
        $id = $data['id'];
        $nrp = htmlspecialchars($data["nrp"]);        // $_POST tidak kita gunakan, karena ketika kita memanggil fungsi ubah dan menggunakan $_POST sebagai parameternya, maka otomatis $_POST akan tersimpan pada $data
        $nama = htmlspecialchars($data["nama"]);      // fungsi htmlspecialchars berguna untuk mengubah kode html yang diinputkan menjadi string biasa, hal ini berguna untuk menghindari ada pengguna jail yang memasukkan kode html melalui kolom input.
        $email = htmlspecialchars($data["email"]);
        $jurusan = htmlspecialchars($data["jurusan"]);
        $gambar = htmlspecialchars($data["gambar"]);

        global $conn;       // Mendeklarasikan variabel $conn agar dapat digunakan di dalam fungsi

        // query insert data ke database
            $query = "UPDATE mahasiswa SET
                    nrp = '$nrp',
                    nama = '$nama', 
                    email = '$email', 
                    jurusan = '$jurusan', 
                    gambar = '$gambar'
                    WHERE id = $id
                    ";
            /**
             * $query = variabel lokal untuk menampung string query
             * INSERT INTO = string query untuk memasukkan data
             * mahasiswa = nama tabel pada database
             * SET = query untuk memasukkan nilai pada tiap field yang dituliskan dibelakangnya/setelahnya 
             * nrp,nama,email,jurusan,gambar = masing-masing field yang nilai didalamnya akan diupdate
             * '' = variabel yang ada didalam kutip adalah variabel yang nilainya akan disimpan dalam field yang telah disebutkan
             * WEERE = untuk memberikan spesifikasi atau ketentuan khusus pada data yang akan diupdate, jika tidak menggunakan WHERE maka operasi update ini akan berjalan pada semua data, sehingga semua datanya akan terubah (bukan hanya baris tertentu)
             * id = atribut yang dibutuhkan WHERE untuk mengidentifikasi baris mana yang akan diupdate
             * $id = Variabel yang berisi nilai id yang tadi sudah ditangkap dengan metode POST saat akan menjalankan fungsi 
             */
            
        
        mysqli_query($conn, $query);
        /**
         * mysqli_query = menjalankan fungsi/sintaks mysql dengan variabel $conn dan $query sebagai parameternya
         * $conn = berisi koneksi database
         * $query = berisi sintaks query
         */

        return mysqli_affected_rows($conn);     // Mengembalikan nilai mysqli_affected_rows
    }
?>