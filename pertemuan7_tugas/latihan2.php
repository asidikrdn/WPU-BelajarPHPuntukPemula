<?php
// cek apakah tidak ada data di $_GET
// isset = apakah variabel sudah dibuat ?
// !isset = apakah variabel belum diuat ?

if( !isset($_GET["gambar"]) ||
    !isset($_GET["nama"]) ||
    !isset($_GET["gender"]) ||
    !isset($_GET["alamat"]) ||
    !isset($_GET["no_hp"])
  ) {
    // jika kosong maka redirect / pindahkan ke halaman lain
    header("Location: latihan1.php");
    exit; // untuk mengakhiri, agar kode selanjutnya tidak di eksekusi
}


?>
<html>
<head>
    <title>Data Pasien</title>
</head>
<body>
    <h1>Data Pasien</h1>

    <ul>
        <li><img src="img/<?= $_GET["gambar"] ?>" width="100px" alt=""></li>
        <li>Nama : <?= $_GET["nama"] ?></li>
        <li>Jenis Kelamin : <?= $_GET["gender"] ?></li>
        <li>Alamat : <?= $_GET["alamat"] ?></li>
        <li>No. Telepon : <?= $_GET["no_hp"] ?></li>
    </ul>

    <a href="latihan1.php">Kembali ke halaman awal</a>
</body>
</html>