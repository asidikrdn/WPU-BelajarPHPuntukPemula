<?php
    $pasien = [
        [
            "nama" => "Ahmad", 
            "gender" => "Pria", 
            "alamat" => "Pasir Kuda, Bogor Barat", 
            "no_hp" => "085155279686",
            "gambar" => "ahmad.jpg"
        ],
        [
            "nama" => "Udin", 
            "gender" => "Pria", 
            "alamat" => "Bogor Nirwana Residence, Bogor Selatan", 
            "no_hp" => "087711356758",
            "gambar" => "udin.jpg"
        ],
        [
            "nama" => "Putri", 
            "gender" => "Wanita", 
            "alamat" => "Rangga Mekar, Bogor Selatan", 
            "no_hp" => "085155279686",
            "gambar" => "putri.jpg"
        ]
    ]; 
?>

<html>
<head>
    <title>GET</title>
</head>
<body>
    <h1>Daftar Pasien</h1>
    
    <ul>
    <?php foreach ($pasien as $ps) : ?>
        <li>
            <a href="latihan2.php?nama=<?= $ps["nama"]; ?>&gender=<?= $ps["gender"]; ?>&alamat=<?= $ps["alamat"]; ?>&no_hp=<?= $ps["no_hp"]; ?>&gambar=<?= $ps["gambar"]; ?>"><?= $ps["nama"];  ?></a>
        </li>
    <?php endforeach; ?>
    </ul>
    
</body>
</html>