<?php
    //definisikan terlebih dahulu, sebab fungsi ini tak terdaftar di sistem bawaan php
    function salam($waktu = "Malam", $nama = "Admin"){
        return "Selamat $waktu, $nama !";
    }
?>
<!--
<html>
<head>
    <title>Latihan Function</title>
</head>
<body>
    <h1><?php echo salam("Pagi","Sidik"); ?></h1>
</body>
</html>
--> 


<html>
<head>
    <title>Latihan Function</title>
</head>
<body>
    <h1><?php echo salam(); ?></h1>
</body>
</html>
