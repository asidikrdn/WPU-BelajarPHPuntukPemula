<?php
    // Date
    // echo date("l, d M Y H:i:s"); // l=hari, d=tanggal, M=bulan(huruf) m=bulan(angka), y=tahun, H=jam, i=menit, s=detik

    // Time
    // UNIX Timetamp / Epoch Timetamp
    // Detik yang sudah berlalu sejak 1 Januari 1970
    //echo time();
    // echo date("l", time()+(60*60*24*2)); //tampilkan 2 hari dari sekarang
    // echo date("l", time()+(60*60*24*100)); //tampilkan 100 hari ke depan dari sekarang
    // echo date("d M Y", time()-(60*60*24*100)); //tampilkan 100 hari ke belakang dari sekarang
    

    // mktme
    // membuat sendiri detik
    // mktime (0,0,0,0,0,0,0)
    // jam, menit, detik, bulan, tanggal, tahun
    // echo mktime(0,0,0,3,14,1999);
    // echo date("l", mktime(0,0,0,3,14,1999));

    // strtotime
    // echo strtotime("14 mar 1999");
    // echo date("l", strtotime("14 mar 1999"));

    // Function yang sering digunakan
    
    /**String
     * strlen()             => menghitung panjang sebuah string
     * strcmp()             => membandingkan 2 buah String
     * explode()            => memecah sebuah string menjadi array
     * htmlspecialchars()   => untuk menjagadari orang iseng ingin masuk ke website 
     */
     
    /**Utility
     * var_dump()           => untuk mencetak isi dari sebuah variabel/object
     * isset()              => untuk mengecek sebuah variabel sudah pernah dibuat atau belum
     * empty()              => untuk mengecek variabel masih kosong atau tidak
     * die()                => untuk memberhentikan program
     * sleep()              => untuk memberhentikan program sementara
     */
?>