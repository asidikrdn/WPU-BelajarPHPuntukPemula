<?php //tinggal ketik php lalu tab

// Pertemuan 2 - PHP Dasar
// Sintaks PHP

// Standar Output
// echo, print (untuk teks dan isi variabel)
// print_r (digunakan untuk array)
// var_dump (digunakan untuk isi variabel)


	// echo "Ahmad Sidik Rudini";
	// echo 'Ahmad Sidik Rudini';
	// echo "<br>";
	// print("Ahmad Sidik Rudini");
	// echo "<br>";
	// print_r("Ahmad Sidik Rudini");
	// echo "<br>";
	// var_dump("Ahmad Sidik Rudini");


// Penulisan sintaks PHP
// 1. PHP di dalam HTML (recomend)
// 2. HTML di dalam PHP


// Variabel dan Tipe Data
// Variabel
// Tidak boleh diawali dengan angka, tapi boleh mengandung angka
	// $nama = "Ahmad Sidik Rudini";

	// echo "<br>";
	// echo 'Halo, nama saya $nama';
	// echo "<br>";
	// echo "Halo, nama saya $nama";
	// echo "<br>";

// Operator
// aritmatika
	// + - * / %
	// $x = 10;
	// $y = 20;

	// echo $x * $y;

// penggabung string / concatenation / concat
//  .
	// $nama_depan = "Ahmad Sidik";
	// $nama_belakang = "Rudini";

	// echo $nama_depan . " " . $nama_belakang;

// Assignment
// operator penugasan
// =, +=, -=, *=, %=, .=
	// $xx = 1;
	// $xx += 5;
	// echo $xx;


	// $nama = "Ahmad";
	// $nama .= " ";
	// $nama .= "Sidik";
	// echo $nama;

// Perbandingan
// <, >, <=, >=, ==, !=
	// var_dump(1 < 5);
	// echo "<br>";
	// var_dump(1 > 5);
	// echo "<br>";
	// var_dump(1 == 5);
	// echo "<br>";
	// var_dump(1 == "1"); //hasilnya true karena var dump tidak membandingkan tipe data, namun hanya membandingkan nilai

// Identitas
// Sama seperti operator perbandingan, namun lebih detil karena membandingkan nilai dan tipe datanya
// ===, !==
	// var_dump(1 === "1");

// Logika
// &&, ||, !
	// $x = 30;

	// var_dump($x < 20 && $x % 2 == 0); // kedua syarat harus terpenuhi
	// var_dump($x < 20 || $x % 2 == 0); // salah satu terpenuhi maka hasilnya true

// Boolean
// Benar Salah
	// $benar = true;
	// $salah = false;
	// echo $benar; // outputnya 1
	// echo $salah; // tidak akan ada outputnya


// Ini komentar 
//(jalan pintasnya tinggal blok text lalu ctrl+/ akan otomatis jadi komentar)

/*
Ini juga Komentar
*/

?>
<!-- Contoh PHP dalam HTML
<!DOCTYPE html>
<html>
<head>
	<title>Pertemuan 2</title>
</head>
<body>
	<h1>Halo, Selamat Datang <?php echo $nama; ?></h1>
</body>
</html>
-->