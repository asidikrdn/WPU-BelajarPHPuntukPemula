<?php
    // Memanggil/menghubungkan dengan file functions.php
        require 'functions.php'; 
        /*
        require = gunakan
        functions.php = file functions.php
        */

    // Ambil data dari tabel mahasiswa / query data mahasiswa menggunakan fungsi query yang ada pada file functions.php    
        $mahasiswa = query("SELECT * FROM mahasiswa");
        /*
        $mahasiswa = membuat variabel array multidimensi
        query = memanggil fungsi query, fungsi disimpan pada file functions.php dan sudah dihubungkan menggunakan require diatas, jadi dapat kita gunakan
        ("SELECT * FROM mahasiswa") = parameter fungsi berbentuk string yang akan disimpan pada variabel $query yang ada pada fungsi query
        */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>

    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Gambar</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Jurusan</th>
        </tr>
        <?php $i = 1;?>
        <?php foreach($mahasiswa as $row) : ?> <!-- Untuk setiap array dalam array multidimensi $mahasiswa menjadi array $row -->
        <tr>
            <td><?= $i ?></td> <!-- mengapa tidak menggunakan id ? karena id pada belum tentu urut, bisa jadi ada yang bolong jika ada baris yang dihapus pada database -->
            <td>
                <a href="">Ubah</a> |
                <a href="">Hapus</a>
            </td>
            <td>
                <img src="img/<?= $row["gambar"] ?>" alt="<?= $row["gambar"] ?>" width="150">
            </td>
            <td><?= $row["nrp"] ?></td>
            <td><?= $row["nama"] ?></td>
            <td><?= $row["email"] ?></td>
            <td><?= $row["jurusan"] ?></td>

            <!-- <?= $row["key"] ?> berfungsi menampilkan data pada variabel row berdasarkan key, data tersebut merupakan data yang diperoleh dari hasil fetch pada tabel mahasiswa pada database phpdasar -->

        </tr>
        <?php $i++ ?> <!-- berfungsi untuk membuat nomor bertambah 1 pada tiap perulangan -->
        <?php endforeach; ?>
    </table>
</body>
</html>