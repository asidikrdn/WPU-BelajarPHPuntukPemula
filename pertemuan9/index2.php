<?php
    //FILE INI MENGGUNAKAN CARA LAMA, CARA BARU DAN MODULAR ADA PADA FILE INDEX

    // Koneksi ke database
        $conn = mysqli_connect("localhost", "root", "", "phpdasar");
        /*
        $conn = untuk menyimpan fungtion koneksi kedalam sebuah variabel agar mudah dipanggil jika dibutuhkan
        mysqli_connect = hubungkan ke mysqli
        () = parameter yang dibutuhkan untuk terhubung ke mysqli
        "localhost" = nama server/host mysqli
        "root" = username mysqli
        "" = password, kosong karena tidak ada password
        "phpdasar" = nama database
        */

    // Ambil data dari tabel mahasiswa / query data mahasiswa
        $result = mysqli_query($conn, "SELECT * FROM mahasiswa");
        /*
        $result = berfungsi untuk menampung data mysqli_query, apabila querynya berhasil maka data akan masuk ke variabel ini, namun apabila querynya gagal maka variabel ini akan berisi nilai "false"
        mysqli_query = menjalankan fungsi/sintaks mysql
        $conn = memanggil database yang akan dijalankan fungsi mysqlnya
        "" = didalam kutip adalah sintak sql yang akan dijalankan
        SELECT = memilih
        * = semua data
        from = dari
        mahasiswa = tabel mahasiswa
        */

    // Untuk menampilkan pesan jika terjadi error saat query data dari database
        if(!$result){
            echo mysqli_error($conn);
        }
        /*
        if = jika
        !$result = tidak result / bukan result / result bernilai false
        echo = cetak
        mysqli_error = pesan error
        $conn = pada koneksi berikut
        */

    // Ambil data (fetch) mahasiswa dari object result
        /*
        Ada 4 cara :
        1. mysqli_fetch_row() = untuk mengambil 1 baris data dan mengembalikan data dalam bentuk array numeric (indeksnya angka)
        2. mysqli_fetch_assoc() = untuk mengambil 1 baris data dan mengembalikan data dalam bentuk array assosiative (indeksnya string sesuai field pada tabel)
        3. mysqli_fetch_array() = untuk mengambil 1 baris data dan mengembalikan data dalam bentuk array numeric (indeksnya angka) dan array assosiative (indeksnya string sesuai field pada tabel)
        4. mysqli_fetch_object() = untuk mengambil 1 baris data dan mengembalikan data dalam bentuk object
        
        Untuk dapat mengambil semua data (bukan hanya satu baris) maka gunakan struktur perulangan,contoh :

        while ($row = mysqli_fetch_assoc($result)) :
            vardump($row) => untuk menampilkan semua array
            vardump($row["key"]) => untuk menampilkan isi array berdasarkan key tertentu
        endwhile;

        $row diatas berfungsi sebagai variabel untuk menampung hasil fetch data dari hasil query yang tersimpan pada variabel result, data yang dihasilkan tergantung dari cara melakukan fetching data (ada 4 cara diatas)
        */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>

    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Gambar</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Jurusan</th>
        </tr>
        <?php $i = 1;?>
        <?php while($row = mysqli_fetch_assoc($result)) : ?>
        <tr>
            <td><?= $i ?></td> <!-- mengapa tidak menggunakan id ? karena id pada belum tentu urut, bisa jadi ada yang bolong jika ada baris yang dihapus pada database -->
            <td>
                <a href="">Ubah</a> |
                <a href="">Hapus</a>
            </td>
            <td>
                <img src="img/<?= $row["gambar"] ?>" alt="<?= $row["gambar"] ?>" width="150">
            </td>
            <td><?= $row["nrp"] ?></td>
            <td><?= $row["nama"] ?></td>
            <td><?= $row["email"] ?></td>
            <td><?= $row["jurusan"] ?></td>

            <!-- <?= $row["key"] ?> berfungsi menampilkan data pada variabel row berdasarkan key, data tersebut merupakan data yang diperoleh dari hasil fetch pada tabel mahasiswa pada database phpdasar -->

        </tr>
        <?php $i++ ?> <!-- berfungsi untuk membuat nomor bertambah 1 pada tiap perulangan -->
        <?php endwhile; ?>
    </table>
</body>
</html>