<?php
    // File ini berisi kumpulan fungsi yang bisa digunakan di file lain dengan memanggil menggunakan require
    // Hal ini dilakukan agar kode lebih rapih dan modular, serta mempercepat penulisan yang membutuhkan fungsi yang sama

    // Koneksi ke database
        $conn = mysqli_connect("localhost", "root", "", "phpdasar");
        /*
        $conn = untuk menyimpan fungtion koneksi kedalam sebuah variabel agar mudah dipanggil jika dibutuhkan
        mysqli_connect = hubungkan ke mysqli
        () = parameter yang dibutuhkan untuk terhubung ke mysqli
        "localhost" = nama server/host mysqli
        "root" = username mysqli
        "" = password, kosong karena tidak ada password
        "phpdasar" = nama database
        */
    
    // Membuat functions query
        function query($query){
            global $conn;
            $result=mysqli_query($conn, $query);
            $rows=[];
            while($row = mysqli_fetch_assoc($result) ) {
                $rows[] = $row;
            }
            return $rows;
        }
        /*
        function = membuat fungsi
        query = nama/identifier fungsinya
        $query = untuk menampung string query, misal "SELECT * FROM mahasiswa" yang akan menjadi parameter saat menggunakan fungsi query
        
        global $conn = mendeklarasikan variabel $conn pada file functions.php agar dapat dibaca juga pada fungsi
        
        $result = berfungsi untuk menampung data mysqli_query, apabila querynya berhasil maka data akan masuk ke variabel ini, namun apabila querynya gagal maka variabel ini akan berisi nilai "false"
        mysqli_query = menjalankan fungsi/sintaks mysql
        $conn = memanggil/menghubungkan database yang akan dijalankan fungsi mysqlnya
        $query = memanggil string query yang tersimpan

        $rows[] = membuat sebuah variabel array baru untuk menampung hasil fetch data secara looping / menampung isi dari variabel $row yang di looping dalam sebuah matriks
        
        while($row = mysqli_fetch_assoc($result)) = struktur looping untuk mengambil data per baris pada database
        $row = berfungsi sebagai variabel untuk menampung hasil fetch data dari hasil query yang tersimpan pada variabel result
        jadi, bedanya $rows dan $row adalah jumlah isinya, jika $row hanya menampung 1 baris, $rows menampung kumpulan $row.
        $rows[] = $row => berfungsi menampung hasil fetch data perbaris menjadi sebuah array

        return $rows = mengembalikan nilai yang ditampung variabel $rows kepada pemanggil fungsi, mengingat variabel yang ada didalam fungsi berbeda / tidak berlaku jika digunakan dalam file php, maka statement 'return' ini berfungsi untuk mengirim nilai yang ditampung pada variabel yang ada didalam fungsi
        */

?>