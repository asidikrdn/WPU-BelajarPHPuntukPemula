<?php
// cek apakah tidak ada data di $_GET

if( !isset($_GET["gambar"]) ||
    !isset($_GET["nama"]) ||
    !isset($_GET["nrp"]) ||
    !isset($_GET["email"]) ||
    !isset($_GET["prodi"])
  ) {
    // redirect / pindahkan ke halaman lain
    header("Location: latihan1.php");
    exit; // untuk mengakhiri, agar kode selanjutnya tidak di eksekusi
}


?>
<html>
<head>
    <title>Data Mahasiswa</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>

    <ul>
        <li><img src="img/<?= $_GET["gambar"] ?>" width="100px" alt=""></li>
        <li>Nama : <?= $_GET["nama"] ?></li>
        <li>NRP : <?= $_GET["nrp"] ?></li>
        <li>Email : <?= $_GET["email"] ?></li>
        <li>Jurusan : <?= $_GET["prodi"] ?></li>
    </ul>

    <a href="latihan1.php">Kembali ke halaman awal</a>
</body>
</html>