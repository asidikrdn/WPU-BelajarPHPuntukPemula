<?php
// Variabel Scope / Lingkup Variabel

// Variabel Lokal

// Variabel global

/*
Variabel Superglobal : Variabel yang sudah dimiliki oleh php yang bisa diakses dimanapun dan kapanpun di halaman php
Variabel Superglobal dalam PHP :
$_GET
$_POST
$_REQUEST
$_SESSION
$_COOKIE
$_SERVER
$_ENV
Semua variabel superglobal tipenya array assosiative
*/
?>

<?php
// Variabel Lokal

// $x = 10;    // variabel lokal untuk halaman latihan1.php

// function tampilx() {
//     $x = 20;
//     echo $x;    // variabel lokal untuk function tampilx
// }

// echo $x;
// echo "<br>";
// tampilx();


?>

<?php
// Variabel Global

// $x = 10;

// function tampilx() {
//     global $x;  // mengambil variabel x dari luar function
//     echo $x;
// }

// tampilx();

?>

<?php
// Variabel $_GET

/*
2 Cara mengisi variabel $_GET

1. Dengan memasukkan di dalam kode php :
    $_GET["nama"] = Ahmad Sidik Rudni;
    $_GET["npm"] = 201106041165;

2. Dengan menambahkan data pada url
    url?key=data&key=data

    ? -> Fungsi untuk memasukkan data pada url
    key -> Key dari data yang akan dimasukkan (sesuai fungsi array assosiative)
    = -> Untuk mendifinisikan data dari key
    data -> Data yang ingin dimasukkan

    data yang dimasukkan pada url tersebut akan disimpan dalam variabel $_GET dengan method GET
*/

// var_dump($_GET);

?>

<?php
    $mahasiswa =[
        [
            "nama" => "Ahmad", 
            "nrp" => "2011060411651", 
            "prodi" => "Teknik Informatika", 
            "email" => "sidikrudini16@gmail.com",
            "gambar" => "ahmad.jpg"
        ],
        [
            "nama" => "Sidik", 
            "nrp" => "2011060411652", 
            "prodi" => "Teknik Informatika", 
            "email" => "sidikrudini16@gmail.com",
            "gambar" => "sidik.jpg",
            // "tugas" => [90, 80, 100]
        ],
        [
            "nama" => "Rudini", 
            "nrp" => "2011060411653", 
            "prodi" => "Teknik Informatika", 
            "email" => "sidikrudini16@gmail.com",
            "gambar" => "rudini.jpg"
        ]
    ];
?>

<html>
<head>
    <title>GET</title>
</head>
<body>
    <h1>Daftar Mahasiswa</h1>
    
    <ul>
    <?php foreach ($mahasiswa as $mhs) : ?>
        <li>
            <a href="latihan2.php?nama=<?= $mhs["nama"]; ?>&nrp=<?= $mhs["nrp"]; ?>&email=<?= $mhs["email"]; ?>&prodi=<?= $mhs["prodi"]; ?>&gambar=<?= $mhs["gambar"]; ?>"><?= $mhs["nama"];  ?></a>
        </li>
    <?php endforeach; ?>
    </ul>
    
</body>
</html>