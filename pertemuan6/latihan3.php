<?php
    // Array Assosiative = Array yang indeksnya menggunakan string yang diasosiasikan
    // "key" 
    // => : untuk mendifinisikan key
    // "Isi key"
    // Array Assosiative bebas mendefinisikan isinya di posisi manapun asalkan indeksnya benar
    $mahasiswa =[
        [
            "nama" => "Ahmad", 
            "nrp" => "2011060411651", 
            "prodi" => "Teknik Informatika", 
            "email" => "sidikrudini16@gmail.com",
            "gambar" => "ahmad.jpg"
        ],
        [
            "nama" => "Sidik", 
            "nrp" => "2011060411652", 
            "prodi" => "Teknik Informatika", 
            "email" => "sidikrudini16@gmail.com",
            "gambar" => "sidik.jpg",
            // "tugas" => [90, 80, 100]
        ],
        [
            "nama" => "Rudini", 
            "nrp" => "2011060411653", 
            "prodi" => "Teknik Informatika", 
            "email" => "sidikrudini16@gmail.com",
            "gambar" => "rudini.jpg"
        ]
    ]; 

    // echo $mahasiswa[2]["nama"];
    // echo $mahasiswa[1]["tugas"][2];
?>

<html>
<head>
    <title>Daftar Mahasiswa</title>
</head>
<body>
    <h1>Daftar Mahasiswa</h1>
    
    <?php foreach ($mahasiswa as $mhs) : ?>
    <ul>
        <li><img src="img/<?= $mhs["gambar"]; ?>" width="100px"  alt=""></li>
        <li>Nama : <?= $mhs["nama"]; ?></li>
        <li>NRP : <?= $mhs["nrp"]; ?></li>
        <li>Jurusan : <?= $mhs["prodi"]; ?></li>
        <li>Email : <?= $mhs["email"]; ?></li>
    </ul>
    <?php endforeach; ?>
</body>
</html>