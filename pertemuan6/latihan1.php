<html>
<!-- Assosiative Array -->

<head>
<title>Latihan 1</title>
<style>
    .kotak {
        width : 30px;
        height : 30px;
        background-color : #bada55;
        text-align : center;
        line-height : 30px;
        margin : 3px;
        float : left;
        transition : 1s;
    }
    .kotak:hover {
        transform : rotate(360deg);
        border-radius : 50%;
    }
    .clear {
        clear: both;
    }
</style>
</head>

<body>

    
    <?php

    $angka =[
                [1,2,3],
                [4,5,6],
                [7,8,9]
            ];
    
    // echo $angka[1][2]; // [1] = indeks array besarnya, [2] = indeks array kecilnya
    ?>

        
    <?php foreach ($angka as $a) : ?>
        <?php foreach ($a as $aa) : ?>
        <div class="kotak"><?= $aa ?></div>
        <?php endforeach; ?>
        <div class="clear"></div>
    <?php endforeach; ?> 
   

</body>
</html>