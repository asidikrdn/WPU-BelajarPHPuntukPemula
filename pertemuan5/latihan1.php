<?php
// Array
// Sebuah variabel yang dapat memiliki banyak nilai
// Elemen pada array boleh memilki tipe data yang berbeda
// pasangan antara key dan value
// key-nya adalah index, yang dimulai dari 0

// $nama = "Sidik";

// $hari1 = "Senin";
// $hari2 = "Selasa";

// Membuat Array cara lama
$hari = array("Senin","Selasa","Rabu");

// Membuat Array cara baru 
$bulan = ["Januari","Februari","Maret"];
$arr1 = [123, "tulisan", true];


// Menampilkan array
// tidak bisa menggunakan echo 
// gunakan var_dump() / print_r()

// echo $hari;

// var_dump($hari);
// echo "<br>";
// print_r($bulan);



// Menampilkan 1 elemen pada array 
// Bisa menggunakan echo 
// echo "<br>";
// echo $arr1[0];
// echo "<br>";
// echo $bulan[1];



// Menambahkan elemen baru pada array 
var_dump($hari);
$hari[] = "Kamis"; //menambah elemen baru
$hari[] = "Jum'at";
echo "<br>";
var_dump($hari);



?>