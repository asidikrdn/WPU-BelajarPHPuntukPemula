<?php
// Array Multidimensi
// Array dalam array

$mahasiswa = [
                ["Ahmad", "2011060411651", "Teknik Informatika", "sidikrudini16@gmail.com"],
                ["Sidik", "2011060411652", "Teknik Informatika", "sidikrudini16@gmail.com"],
                ["Rudini", "2011060411653", "Teknik Informatika", "sidikrudini16@gmail.com"],

            ];
?>

<html>
<head>
    <title>Daftar Mahasiswa</title>
</head>
<body>

<h1>Daftar Mahasiswa</h1>

<?php foreach ($mahasiswa as $mhs) : ?>
<ul>
    <!--
    <?php foreach ($mhs as $m) : ?>
        <li><?php echo $m; ?></li>
    <?php endforeach; ?>
    -->
    <li>Nama  :<?php echo $mhs[0];?></li>
    <li>NPM   :<?php echo $mhs[1];?></li>
    <li>Prodi :<?php echo $mhs[2];?></li>
    <li>Email :<?php echo $mhs[3];?></li>
</ul>
<?php endforeach; ?>

</body>
</html>