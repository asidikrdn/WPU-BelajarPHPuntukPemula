<?php
// Pengulangan pada array
// for / foreach

$angka = [1,2,5,8,7,9,54,72,111,99];

?>

<html>
<head>
    <title>Latihan 2</title>
    <style>
    .kotak {
        width: 50px;
        height: 50px;
        background-color: salmon;
        text-align: center;
        line-height: 50px;
        margin: 3px;
        float: left;
    }
    .clear {clear: both;}
    </style>
</head>
<body>

<!-- Menggunakan For -->
<?php for($i=0; $i < count($angka); $i++) { ?>   <!-- Fungsi count() adalah untuk menghitung otomatis panjang arraynya -->
<div class="kotak"><?php echo $angka[$i]; ?></div>
<?php } ?>

<div class="clear"></div>

<!-- Menggunakan Foreach -->
<?php foreach($angka as $a) { ?>   <!-- Foreach : Untuk Setiap, (array as variabel sementara untuk menampung elemen array yang dipanggil) -->
<div class="kotak"><?php echo $a; ?></div>
<?php } ?>

<div class="clear"></div>

<!-- Menggunakan Foreach Penulisan Templating-->
<?php foreach($angka as $a) : ?>   <!-- Foreach : Untuk Setiap, (array as variabel sementara untuk menampung elemen array yang dipanggil) -->
<div class="kotak"><?= $a; ?></div>
<?php endforeach; ?>




</body>

</html>